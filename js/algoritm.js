'use strict'

Object.defineProperty(Array.prototype, 'sortInsert', {
    value: function() {
        for ( let i = 0; i < this.length; i++ ) {
            let index = i;
            let value = this[i];

            while ( index > 0 && this[index - 1] > value ) {
                this[index] = this[index - 1];
                index--;
            }
            
            this[index] = value;
        }
        return this;
    }
});

Object.defineProperty(Array.prototype, 'sortShel', {
    value: function() {
        let halfArr = Math.floor(this.length / 2);

        while ( halfArr > 0 ) {
        
            for ( let j = 0; j < this.length; j++ ) {
                let index = j;
                let value = this[j];
    
                while ( index >= halfArr && this[index - halfArr] > value ) {
                    this[index] = this[index - halfArr];
                    index -= halfArr;
                }
    
                this[index] = value;
            }
    
            halfArr = (halfArr / 2);
        }
        return this;
    }
});

Object.defineProperty(Array.prototype, 'bubbleSort', {
    value: function(callBack) {

        for ( let i = 0; i < this.length - 1; i++ ) {
            for ( let j = 0; j < this.length - 1; j++ ) {

                if ( callBack(this[i], this[j]) > 0) {
                    let previousNumber = this[i];
                    this[i] = this[j];
                    this[j] = previousNumber;
                }
            }
        }
        return this;
    }
});

class Node {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
}

class Tree {
    constructor() {
        this.root = null;
    }

    insert(value) {
        let newNode = new Node(value);

        if ( !this.root ) {
            this.root = newNode;
            return;
        }

        let root = this.root;

        while ( root ) {
            if ( newNode.value < root.value ) {
                if ( !root.left ) {
                    root.left = newNode;
                    return;
                }

                root = root.left;
            } else {
                if (!root.right) {
                    root.right = newNode;
                    return;
                }

                root = root.right;
            }
        }
    }

    research(element, callBack) {

        if ( !element ) {
            return;
        }

        this.research(element.left, callBack);

        if ( callBack ) {
            callBack(element);
        }
        this.research(element.right, callBack);

    }

    sort(callBack, metod) {

        if ( metod === 'research' ) {
            return this.research(this.root, callBack);
        }
    }

    search(element, value) {

        if ( element === null ) {
            return null;
        } else if ( value < element.value ) {
            return this.search(element.left, value);
        } else if  ( value > element.value ) {
            return this.search(element.right, value)
        } 

        return element;
    }

    smollestElem(element) {
        if ( element.left === null )
        return element;
    }

    remove(element, value) {

        if ( element === null ) {
            return null;
        } else if ( value < element.value ) {
            element.left = this.remove(element.left, value);
            return element;
        } else if ( value > element.value ) {
            element.right = this.remove(element.right, value);
            return element;
        } else {
            if ( element.left === null && element.right === null ) {
                element = null;
                return element;
            }

            if ( element.left === null ) {
                element = element.right;
                return element;
                
            } else if ( element.right === null ) {
                element = element.left;
                return element
            }
        }
        let newNode = this.smollestElem(element.right);
        element.value = newNode.value;
        element.right = this.remove(element.right, newNode.value);
        return element;
    }
    
}

let binarTree = new Tree();

binarTree.insert(20);
binarTree.insert(7);
binarTree.insert(8);
binarTree.insert(15);
binarTree.insert(35);
binarTree.insert(4);
binarTree.insert(21);
binarTree.insert(2);
binarTree.insert(32);
binarTree.insert(62);

binarTree.remove(binarTree.root, 35)
binarTree.search(binarTree.root, 2)
