'use strict'

// 12, 13,  9 task summ

let anagram = (firstlLine, secondLine) => {
   let string = '';

   if (firstlLine.length == secondLine.length) {
      for (let i = 0; i < firstlLine.length; i++) {
         for (let j = 0; j < secondLine.length; j++) {
            if (firstlLine[i] == secondLine[j]) {
               string += firstlLine[i]
               break;
            }
         }
      }
   } else {
      return false;
   }

   if (firstlLine == string) {
      return true;
   }

   return false;
}

let findEqualNumbers = (num) => {
   let currentNumers = num.split('');
   let repeaters = {};

   for (let i in currentNumers) {
      repeaters[currentNumers[i]] = repeaters[currentNumers[i]] || 0;
      repeaters[currentNumers[i]]++;
   }

   return repeaters;
}

let numberOfUniqueWords = (sentence) => {
   let currentWords = sentence.split(' ');
   let obj = {};

   for (let i in currentWords) {
      obj[currentWords[i]] = obj[currentWords[i]] || 0;
      obj[currentWords[i]]++;
   }

   let arrayOfValues = Object.values(obj);
   let unique = 0;

   for (let i = 0; i < arrayOfValues.length; i++) {
      if (arrayOfValues[i] == 1) {
         unique += 1;
      }
   }
   return unique;
}

let findEqualWords = (sentence) => {
   let currentWords = sentence.split(' ');
   let repeaters = {};

   for (let i in currentWords) {
      repeaters[currentWords[i]] = repeaters[currentWords[i]] || 0;
      repeaters[currentWords[i]]++;
   }

   return repeaters;
}

let fibonachiCount = (n) => {
   let fibonachi = [0, 1];

   for (let i = 2; i < n; i++) {
      fibonachi[i] = fibonachi[i - 1] + fibonachi[i - 2];
   }
   return fibonachi;
}

function calculationRectanglePerimetr(sideOne, sideTwo) {
   return (sideOne + sideTwo) * 2;
}

function calculationRectangleSquere(sideOne, sideTwo) {
   return sideOne * sideTwo;
}

function calculationTrianglePerimetr(sideOne, sideTwo, sideThree) {
   return sideOne + sideTwo + sideThree;
}

function calculationTriangleSquere(sideOne, sideThree) {
   return (sideOne * sideThree) / 2;
}

function calculationCirclePerimetr(sideOne) {
   return 2 * Math.PI * sideOne;
}

function calculationCirсleSquere(sideOne) {
   return (2 * Math.PI * sideOne * (sideOne * 2)) / 4;
}

function factorial(n) {
   let total = 1;
   for (i = 0; i < n; i++) {
      total = total + (n - 1)
   }
   return total;
}

Array.prototype.sum = function(fn) {
   let mult = [];

   for (let i = 0; i < this.length; i++) {
      if (fn(this[i])) {
         mult.push(this[i])
      }
   }

   let sum = 0;
   for (let i = 0; i < mult.length; i++) {
      sum += mult[i]
   }

   return sum;
}

// let fuf = [-1,-2,-3,-4,1,2,3,4, 39, 45, 22, -66, 43];
// console.log(fuf.sum(a => a % 2 === 0));
// console.log(fuf.sum(a => a % 3 === 0));
// console.log(fuf.sum(a => a > 0 && a % 2));

Array.prototype.amountOF = function(fn, aim) {
   aim = aim || 0;

   for (let i = 0; i < this.length; i++) {
      if (fn(this[i])) {
         aim++
      }
   }

   return aim;
}

// let faf = [1, 4, 5, 0, -3, -6, 1, 4, -5, 0, 1, 5, 3, 2, 0, 6, 14, 17, -453, 6547, -123, 0];
// console.log(faf.amountOF(a => a == 0));
// console.log(faf.amountOF(a => a < 0));
// console.log(faf.amountOF(a => a > 0));

let shiftingToBinary = (num) => {
   let out = [];
   let bit = 1;

   while (num >= bit) {
      out = (num & bit ? 1 : 0) + out;
      bit <<= 1;
   }

   return out || "0";
}

let shiftingToDecimal = (num) => {
   let out = 0;
   let len = num.length;
   let bit = 1;

   while (len--) {
      out += num[len] == 1 ? bit : 0;
      bit <<= 1;
   }

   return out;
}

let sumPositiveOddNumInTwoDementionArray = (num) => {
   let res = [];

   for (let i = 0; i < num.length; i++) {
      for (let j = 0; j < num[i].length; j++) {
         if (num[i][j] % 2 === 0 && num[i][j] > 0) {
            res.push(num[i][j]);
         }
      }
   }

   let sum = 0;
   for (let i = 0; i < res.length; i++) {
      sum += res[i];
   }

   return sum;
}

let findSumMultiplesInTwodementionArray = (numbers, num) => {
   let multiples = [];

   for (let i = 0; i < numbers.length; i++) {
      for (let j = 0; j < numbers[i].length; j++) {
         if (numbers[i][j] % num === 0) {
            multiples.push(numbers[i][j]);
         }
      }
   }

   let sum = 0;

   for (let i = 0; i < multiples.length; i++) {
      sum += multiples[i];
   }

   return sum;
}

let sumOfNumbersInTwoDementionsArray = (num) => {
   let res = {
      zeros: 0,
      negative: 0,
      positive: 0,
      simple: 0,
   }

   for (let i = 0; i < num.length; i++) {
      for (let j = 0; j < num[i].length; j++) {
         if (num[i][j] === 0) {
            res.zeros++;
         }
         if (num[i][j] < 0) {
            res.negative++;
         }
         if (num[i][j] > 0) {
            res.positive++;
         }
         if (!(num[i][j] % 2 && num[i][j] % 3 && num[i][j] % 5 && num[i][j] % 7)) {
            res.simple++;
         }
      }
   }

   return res;
}

Array.prototype.minMaxSumm = function(fn) { 
   let res = [];
   let i = 0;

   for (let num in this) {
      if (fn(this[num])) {
         res[i] = this[num]
         i++;
      }
   }
   return res
}

let rangeOfNumbers = [];
for (let i = -45; i <= 30; i++) {
   rangeOfNumbers.push(i)
}

// console.log(rangeOfNumbers.minMaxSumm(a => a % 3 === 0));
// console.log(rangeOfNumbers.minMaxSumm(a => a > 0));


let theAverageOfArrayElement = (mass) => {
   let massFlat = mass.flat()
   let resultMean = {
      even: 0,
      notEven: 0,
   }

   for (let i = 0; i < massFlat.length; i++) {
      if (massFlat[i] % 2 == 0) {
         resultMean.even += massFlat[i] / 2;
      } else {
         resultMean.notEven += massFlat[i] / 2;
      }
   }

   return resultMean;
}

let transposeMatrix = (matrix) => {
   let transpose = [];

   for (let i = 0; i < matrix[0].length; i++) {
      let arr = [];
      for (let j = 0; j < matrix.length; j++) {
         arr.push(matrix[j][i]);
      }
      transpose.push(arr);
   }
   return transpose;
}

function sumMatrix(matrixOne, matrixTwo) {
   let res = [];

   for (let i = 0; i < matrixOne.length; i++) {
      res[i] = [];
      for (let j = 0; j < matrixOne[0].length; j++) res[i][j] = matrixOne[i][j] + matrixTwo[i][j];
   }

   return res;
}

let deleteMatrixRowWithZeros = (matrix) => {
   for (let i = 0; i < matrix.length; i++) {
      for (let j = 0; j < matrix[i].length; j++) {
         if (matrix[i][j] == 0) {
            matrix.splice(i, 1);
            break;
         }
      }
   }
   return matrix;
}

let deleteMtarixColumWithZeros = (matrix) => {
   let deleteСolumn = (j) => {
      for (let i = 0; i < matrix.length; i++) {
         matrix[i].splice(j, 1);
      }
   }

   for (let i = 0; i < matrix.length; i++) {
      for (let j = 0; j < matrix[i].length; j++) {
         if (matrix[i][j] == 0) {
            deleteСolumn(j);
            break;
         }
      }
   }

   return matrix;
}
