'use strict'

Object.defineProperty(Array.prototype, 'itsMyPush', {
   value: function (...allarg) {
      for (let i = 0; i < allarg.length; i++) {
         this[this.length] = allarg[i];
      }

      return this.length;
   }
});
// push для следующих заданий

Object.defineProperty(Array.prototype, 'itsMyyMap', {
   value: function (fn) {
      let arr = [];

      for (let i = 0; i < this.length; i++) {
         arr.itsMyPush(fn(this[i], i, this));
      }

      return arr;
   }
});

Object.defineProperty(Array.prototype, 'itsMyFillter', {
   value: function (fn) {
      const filtered = [];

      for (let i = 0; i < this.length; i++) {
         if (fn(this[i])) {
            filtered.itsMyPush(this[i]);
         }
      }

      return filtered;
   }
});

Object.defineProperty(Array.prototype, 'itsMyCall', {
   value: function (compare, ...allarg) {
      let symbol = Symbol();
      compare[symbol] = this;
      let result = compare[symbol](...allarg);
      delete compare[symbol];
      return result;
   }
});

Object.defineProperty(Array.prototype, 'itsMyBind', {
   value: function (compare, ...allarg) {
      let object = {
         ...compare,
      };
      let symbol = Symbol();
      object[symbol] = this;

      return function (...rest) {
         let result = object[symbol](...allarg, ...rest);
         delete object[symbol];
         return result;
      };
   }
});

Object.defineProperty(Array.prototype, 'itsMyForEach', {
   value: function (compare, index) {
      index = index || 0;

      if (index < this.length) {
         compare(this[index], this);
         this.itsMyForEach(compare, ++index);
      };
   }
});

Object.defineProperty(Array.prototype, 'itsMyReduce', {
   value: function (cb, accumulator) {
      let startIndex = 0;
      if (accumulator == undefined) {
         startIndex = 1;
         accumulator = this[0];
      }
      for (let i = startIndex; i < this.length; i++) {
         accumulator = cb(accumulator, this[i], i, this);
      }
      return accumulator
   }
});

function* fibonachiGenerator(n, current, next) {
   current = current || 0;
   next = next || 1;

   if (n == 0) {
      return current;
   }

   yield current
   yield* fibonachiGenerator(n - 1, next, current + next);
};

let fibonachiIterator = {
   a: 0,
   b: 1,
   n: 10,
   [Symbol.iterator]() {
      let current = this.a;
      let next = this.b;
      let n = this.n;
      let arr = [current, next];
      return {
         next() {
            arr.itsMyPush(arr[current] + arr[next++])
            current++;
            return {
               value: arr.length <= n ? arr : undefined,
               done: current > n,
            };
         }
      };
   }
};