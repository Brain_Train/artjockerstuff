'use strict'

let memoizer = (func) => {
   let cache = {};

   return (...n) => {
      if (n in cache) {
         return cache[n];
      }

      return cache[n] = func(...n);
   };
}

let memoRecutsionFindEqualNumbers = memoizer(function (nums, index, obj) {
   index = index || 0;
   obj = obj || {};
   let massNums = nums.toString().split('');

   if (massNums.length > index) {
      obj[massNums[index]] = ++obj[massNums[index]] || 1;
      return memoRecutsionFindEqualNumbers(nums, ++index, obj)
   }

   return obj;
});

let memoRecutsionNumberOfUniqueWords = memoizer(function (str, index, obj) {
   index = index || 0;
   obj = obj || {};
   let strings = str.split(' ');

   if (strings.length > index) {
      obj[strings[index]] = ++obj[strings[index]] || 1;
      return memoRecutsionNumberOfUniqueWords(str, ++index, obj);
   }
   let arrayOfValues = Object.values(obj);
   let counter = 0;

   for (let i = 0; i < arrayOfValues.length; i++) {
      if (arrayOfValues[i] === 1) {
         counter += 1;
      }
   }

   return counter;
});

let memoRecutsionFindEqualWords = memoizer(function (str, index, obj) {
   index = index || 0;
   obj = obj || {};
   let strings = str.split(' ');

   if (strings.length > index) {
      obj[strings[index]] = ++obj[strings[index]] || 1;
      memoRecutsionFindEqualWords(str, ++index, obj);
   }

   return obj;
});

let memoFibonachi = memoizer(function (n, memo) {
   memo = memo || {};

   if (memo[n]) {
      return memo[n];
   }
   if (n <= 1) {
      return 1;
   }

   return memo[n] = memoFibonachi(n - 1, memo) + memoFibonachi(n - 2, memo);
});

let memoRecursionFactorial = memoizer(function (n) {
   if (n === 1) {
      return 1;
   }
   return n * memoRecursionFactorial(n - 1);
});

let memoAmountOfZero = memoizer(function (mass, zero, index) {
   zero = zero || 0;
   index = index || 0;

   if (mass.length > index) {
      if (mass[index] == 0) {
         zero++;
         return memoAmountOfZero(mass, zero, ++index);
      } else {
         zero = memoAmountOfZero(mass, zero, ++index);
      }
   }

   return zero;
});

let memoAmountOfNegotives = memoizer(function (mass, negative, index) {
   negative = negative || 0;
   index = index || 0;

   if (mass.length > index) {
      if (mass[index] < 0) {
         negative++;
         return memoAmountOfNegotives(mass, negative, ++index);
      } else {
         negative = memoAmountOfNegotives(mass, negative, ++index)
      }
   }

   return negative;
});

let memoAmountOfPositives = memoizer(function (mass, positive, index) {
   positive = positive || 0;
   index = index || 0;

   if (mass.length > index) {
      if (mass[index] > 0) {
         positive++;
         return memoAmountOfPositives(mass, positive, ++index);
      } else {
         positive = memoAmountOfPositives(mass, positive, ++index)
      }
   }

   return positive;
});

let memoRecursionFindSumMultiplesInTwoDementionArray = memoizer(function (mass) {
   let arr = [];
   let result = 0;

   let parseArray = (mass) => {
      for (let item in mass) {
         if (typeof mass[item] === "object") {
            parseArray(mass[item]);
         } else {
            arr.push(mass[item]);
         }
      }
   }

   parseArray(mass)
   for (let index in arr) {
      if (arr[index] % 2 == 0) {
         result += arr[index]
      }
   }

   return result;
});

let memoRecursionFindSumMultiplesInthreeDementionArray = memoizer(function (mass) {
   let arr = [];
   let result = 0;

   let parseArray = (mass) => {
      for (let item in mass) {
         if (typeof mass[item] === "object") {
            parseArray(mass[item]);
         } else {
            arr.push(mass[item]);
         }
      }
   }

   parseArray(mass)
   for (let index in arr) {
      if (arr[index] % 3 == 0 && arr[index] !== 0) {
         result += arr[index]
      }
   }

   return result;
});

let memoRecursionSumPositiveNumbersDimensionalArray = memoizer(function (mass) {
   let arr = [];
   let result = 0;

   let parseArray = (mass) => {
      for (let item in mass) {
         if (typeof mass[item] === "object") {
            parseArray(mass[item]);
         } else {
            arr.push(mass[item]);
         }
      }
   }

   parseArray(mass)
   for (let index in arr) {
      if (arr[index] > 0) {
         result += arr[index]
      }
   }

   return result;
});

let memoRecursionSumPositiveOddNumInTwoDementionArray = memoizer(function (mass) {
   let arr = [];
   let result = 0;

   let parseArray = (mass) => {
      for (let item in mass) {
         if (typeof mass[item] === "object") {
            parseArray(mass[item]);
         } else {
            arr.push(mass[item]);
         }
      }
   }

   parseArray(mass)
   for (let index in arr) {
      if (arr[index] % 2) {
         result += arr[index]
      }
   }

   return result;
});

let memoRecursionAllMinMaxSumm = memoizer(function (min, max) {
   let sum = 0;

   if (++min <= max) {
      sum = memoRecursionAllMinMaxSumm(min, max) + min;

   }
   return sum;

});

let memoRecursionMinMaxSumMultiples = memoizer(function (min, max, sum) {
   sum = sum || 0;

   if (++min <= max) {
      if (min % 3 == 0) {
         sum += min;
      }
      sum = memoRecursionMinMaxSumMultiples(min, max, sum)
   }

   return sum;
});

let memoRecirsionPositiveMinMaxSumElements = memoizer(function (min, max, sum) {
   sum = sum || 0;

   if (++min <= max) {
      if (min > 0) {
         sum += min;
      }
      sum = memoRecirsionPositiveMinMaxSumElements(min, max, sum)
   }

   return sum;
});

let memoRecursiveTheAverageOfArrayElement = memoizer(function (mass) {
   let massFlat = mass.flat();
   let res = {
      even: 0,
      odd: 0,
   };

   let parsingArray = (massFlat, counter) => {
      counter = counter || 0
      if (massFlat.length > counter) {
         if (massFlat[counter] % 2 == 0) {
            res.even += massFlat[counter] / 2;
         } else {
            res.odd += massFlat[counter] / 2;
         }
         parsingArray(massFlat, ++counter)
      }
   }

   parsingArray(massFlat);
   return res;
});

let memoRecusionSumMatrix = memoizer(function (matricesOne, matricesTwo, count, result) {
   count = count || 0;
   result = result || [
      [0, 0, 0],
      [0, 0, 0],
      [0, 0, 0]
   ];

   if (count < matricesOne.length) {
      for (let i in matricesOne[count]) {
         result[count][i] += matricesOne[count][i] + matricesTwo[count][i];
      }
      memoRecusionSumMatrix(matricesOne, matricesTwo, ++count, result)
   }

   return result;
});

let memoRecurtionDeleteMatrixRowWithZeros = memoizer(function (matrix, index) {
   index = index || 0;

   if (index++ < matrix.length) {
      for (let j in matrix[index]) {
         if (matrix[index][j] == 0) {
            matrix.splice(index, 1);
            break;
         }
      }
      memoRecurtionDeleteMatrixRowWithZeros(matrix, index);
   }
   return matrix;
});

let memoRecursionDeleteMtarixColumWithZeros = memoizer(function (mass, index) {
   index = index || 0;

   let deleteСolumn = (j) => {
      for (let index in mass) {
         mass[index].splice(j, 1);
      }
   }
   if (index++ < mass.length) {
      for (let j in mass[index]) {
         if (mass[index][j] == 0) {
            deleteСolumn(j);
            break;
         }
      }
      memoRecursionDeleteMtarixColumWithZeros(mass, index);
   }
   return mass;
});
