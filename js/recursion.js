'use strict'

let recutsionFindEqualNumbers = (nums, index, obj) => {
   index = index || 0;
   obj = obj || {};
   let massNums = nums.toString().split('');

   if (massNums.length > index) {
      obj[massNums[index]] = ++obj[massNums[index]] || 1;
      return recutsionFindEqualNumbers(nums, ++index, obj)
   }

   return obj;
}

let recutsionNumberOfUniqueWords = (str, index, obj) => {
   index = index || 0;
   obj = obj || {};
   let strings = str.split(' ');

   if (strings.length > index) {
      obj[strings[index]] = ++obj[strings[index]] || 1;
      return recutsionNumberOfUniqueWords(str, ++index, obj);
   }
   let arrayOfValues = Object.values(obj);
   let counter = 0;

   for (let i = 0; i < arrayOfValues.length; i++) {
      if (arrayOfValues[i] === 1) {
         counter += 1;
      }
   }

   return counter;
}

let recutsionFindEqualWords = (str, index, obj) => {
   index = index || 0;
   obj = obj || {};
   let strings = str.split(' ');

   if (strings.length > index) {
      obj[strings[index]] = ++obj[strings[index]] || 1;
      recutsionFindEqualWords(str, ++index, obj);
   }

   return obj;
}

let recutsionFibonacciNumber = (cycle, index, fibonachi) => {
   cycle = cycle || 0;
   index = index || 0;
   fibonachi = fibonachi || [0, 1];

   if (cycle / 2 > index) {
      fibonachi[0] = fibonachi[0] + fibonachi[1], fibonachi[1] = fibonachi[0] + fibonachi[1];
      recutsionFibonacciNumber(cycle, ++index, fibonachi);
   }

   return fibonachi;
}

function recursionFactorial(n) {
   if (n === 1) {
      return 1;
   } else {
      return n * recursionFactorial(n - 1)
   }
}

let recursionAnountOfZero = (mass, zero, index) => {
   zero = zero || 0;
   index = index || 0;

   if (mass.length > index) {
      if (mass[index] == 0) {
         zero++;
         return recursionAnountOfZero(mass, zero, ++index);
      } else {
         zero = recursionAnountOfZero(mass, zero, ++index);
      }
   }

   return zero;
}

let recursionAmountOfNegative = (mass, negative, index) => {
   negative = negative || 0;
   index = index || 0;

   if (mass.length > index) {
      if (mass[index] < 0) {
         negative++;
         return recursionAmountOfNegative(mass, negative, ++index);
      } else {
         negative = recursionAmountOfNegative(mass, negative, ++index)
      }
   }

   return negative;
}

let recursionAmountOfPositive = (mass, positive, index) => {
   positive = positive || 0;
   index = index || 0;

   if (mass.length > index) {
      if (mass[index] > 0) {
         positive++;
         return recursionAmountOfPositive(mass, positive, ++index);
      } else {
         positive = recursionAmountOfPositive(mass, positive, ++index)
      }
   }

   return positive;
}

let recursionFindSumMultiplesInTwoDementionArray = (mass) => {
   let arr = [];
   let result = 0;

   let parseArray = (mass) => {
      for (let item in mass) {
         if (typeof mass[item] === "object") {
            parseArray(mass[item]);
         } else {
            arr.push(mass[item]);
         }
      }
   }

   parseArray(mass)
   for (let index in arr) {
      if (arr[index] % 2 == 0) {
         result += arr[index]
      }
   }

   return result;
}

let recursionFindSumMultiplesInthreeDementionArray = (mass) => {
   let arr = [];
   let result = 0;

   let parseArray = (mass) => {
      for (let item in mass) {
         if (typeof mass[item] === "object") {
            parseArray(mass[item]);
         } else {
            arr.push(mass[item]);
         }
      }
   }

   parseArray(mass)
   for (let index in arr) {
      if (arr[index] % 3 == 0 && arr[index] !== 0) {
         result += arr[index]
      }
   }

   return result;
}

let sumPositiveNumbersDimensionalArray = (mass) => {
   let arr = [];
   let result = 0;

   let parseArray = (mass) => {
      for (let item in mass) {
         if (typeof mass[item] === "object") {
            parseArray(mass[item]);
         } else {
            arr.push(mass[item]);
         }
      }
   }

   parseArray(mass)
   for (let index in arr) {
      if (arr[index] > 0) {
         result += arr[index]
      }
   }

   return result;
}

let recursionSumPositiveOddNumInTwoDementionArray = (mass) => {
   let arr = [];
   let result = 0;

   let parseArray = (mass) => {
      for (let item in mass) {
         if (typeof mass[item] === "object") {
            parseArray(mass[item]);
         } else {
            arr.push(mass[item]);
         }
      }
   }

   parseArray(mass)
   for (let index in arr) {
      if (arr[index] % 2) {
         result += arr[index]
      }
   }

   return result;
}

let recursionAllMinMaxSumm = (min, max) => {
   let sum = 0;

   if (++min <= max) {
      sum = recursionAllMinMaxSumm(min, max) + min;

   }
   return sum;

}

let recursionMinMaxSumMultiples = (min, max, sum) => {
   sum = sum || 0;

   if (++min <= max) {
      if (min % 3 == 0) {
         sum += min;
      }
      sum = recursionMinMaxSumMultiples(min, max, sum)
   }

   return sum;
}

let recirsionPositiveMinMaxSumElements = (min, max, sum) => {
   sum = sum || 0;

   if (++min <= max) {
      if (min > 0) {
         sum += min;
      }
      sum = recirsionPositiveMinMaxSumElements(min, max, sum)
   }

   return sum;
}

let recursiveTheAverageOfArrayElement = (mass) => {
   let massFlat = mass.flat();
   let res = {
      even: 0,
      odd: 0,
   };

   let parsingArray = (massFlat, counter) => {
      counter = counter || 0
      if (massFlat.length > counter) {
         if (massFlat[counter] % 2 == 0) {
            res.even += massFlat[counter] / 2;
         } else {
            res.odd += massFlat[counter] / 2;
         }
         parsingArray(massFlat, ++counter)
      }
   }

   parsingArray(massFlat);
   return res;
}

let recusionSumMatrix = (matricesOne, matricesTwo, count, result) => {
   count = count || 0;
   result = result || [
      [0, 0, 0],
      [0, 0, 0],
      [0, 0, 0]
   ];

   if (count < matricesOne.length) {
      for (let i in matricesOne[count]) {
         result[count][i] += matricesOne[count][i] + matricesTwo[count][i];
      }
      recusionSumMatrix(matricesOne, matricesTwo, ++count, result)
   }

   return result;
}

let recurtionDeleteMatrixRowWithZeros = (matrix, index) => {
   index = index || 0;

   if (index++ < matrix.length) {
      for (let j in matrix[index]) {
         if (matrix[index][j] == 0) {
            matrix.splice(index, 1);
            break;
         }
      }
      recurtionDeleteMatrixRowWithZeros(matrix, index);
   }
   return matrix;
}

let recursionDeleteMtarixColumWithZeros = (mass, index) => {
   index = index || 0;

   let deleteСolumn = (j) => {
      for (let index in mass) {
         mass[index].splice(j, 1);
      }
   }
   if (index++ < mass.length) {
      for (let j in mass[index]) {
         if (mass[index][j] == 0) {
            deleteСolumn(j);
            break;
         }
      }
      recursionDeleteMtarixColumWithZeros(mass, index);
   }
   return mass;
}
